/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

/**
 *
 * @author mrnata97
 */
public class TP2Ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Comienza la impresion de los numeros del 1 al 100.000");
        Cronometro crono = new Cronometro();
        for(int i=1; i <100001 ; i++){
            System.out.print(i + " ");
        }
        crono.detiene();
        System.out.println();
        System.out.println("Tiempo transcurrido " + crono.tiempoTranscurrido() + " milisegundos.");
    }
    
}
