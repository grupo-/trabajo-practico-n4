/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;
//import java.sql.Time;
import java.util.Date;
/**
 *
 * @author mrnata97
 */
public class Cronometro {
        private Date horaInicio;
        private Date horaFinal;
        
        public Cronometro (){
            this.horaInicio = new Date();
        }
        public void comienza (){
            this.horaInicio =  new Date();
        }
        public void detiene(){
            this.horaFinal = new Date();
        }
        public long tiempoTranscurrido () {
                return (this.horaFinal.getTime()- this.horaInicio.getTime());
        }
        
}